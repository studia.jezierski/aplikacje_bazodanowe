package com.example.aplikacje_bazodanowe;

import com.example.aplikacje_bazodanowe.student.Student;
import com.example.aplikacje_bazodanowe.user.Role;
import com.example.aplikacje_bazodanowe.user.User;
import com.example.aplikacje_bazodanowe.user.UserService;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;

@SpringBootApplication
@RestController
public class AplikacjeBazodanoweApplication {

	public static void main(String[] args) {
		SpringApplication.run(AplikacjeBazodanoweApplication.class, args);
	}

	@Bean
	PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	CommandLineRunner run(UserService userService) {
		return args -> {
			userService.saveRole(new Role(null, "USER"));
			userService.saveRole(new Role(null, "ADMIN"));

			userService.saveUser(new User(null, "John", "john@example.com", "john", "admin1234", new ArrayList<>()));
			userService.saveUser(new User(null, "Admin", "admin@example.com", "admin", "admin1234", new ArrayList<>()));

			userService.addRoleToUser("john", "USER");
			userService.addRoleToUser("admin", "ADMIN");
		};
	}
}
