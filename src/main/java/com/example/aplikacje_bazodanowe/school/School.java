package com.example.aplikacje_bazodanowe.school;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.Period;

@Entity
@Table
public class School {
    @Id
    @SequenceGenerator(
            name = "school_sequence",
            sequenceName = "school_sequence",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "school_sequence"
    )
    private Long id;



    private Number number;
    private String name;
    private String city;
    private LocalDate foundedDate;

    public School() {
    }

    public School(Long id, Number number, String name, String city, LocalDate foundedDate) {
        this.id = id;
        this.number = number;
        this.name = name;
        this.city = city;
        this.foundedDate = foundedDate;
    }

    public School(Number number, String name, String city, LocalDate foundedDate) {
        this.number = number;
        this.name = name;
        this.city = city;
        this.foundedDate = foundedDate;
    }

    public Long getId() {
        return id;
    }

    public Number getNumber() {
        return number;
    }

    public String getName() {
        return name;
    }

    public String getCity() {
        return city;
    }

    public LocalDate getFoundedDate() {
        return foundedDate;
    }

//    public void setId(Long id) {
//        this.id = id;
//    }

    public void setNumber(Number number) {
        this.number = number;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setFoundedDate(LocalDate foundedDate) {
        this.foundedDate = foundedDate;
    }

    @Override
    public String
    toString() {
        return "School{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", city='" + city + '\'' +
                ", foundedDate=" + foundedDate +
                '}';
    }
}
