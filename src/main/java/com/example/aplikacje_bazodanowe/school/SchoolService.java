package com.example.aplikacje_bazodanowe.school;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SchoolService {

    @Autowired
    private final SchoolRepository schoolRepository;

    public SchoolService(SchoolRepository schoolRepository) {
        this.schoolRepository = schoolRepository;
    }

    public List<School> getSchools() {
        return schoolRepository.findAll();
    }

    public void addNewSchool(School school) {
        Optional<School> existingSchool = schoolRepository.findSchoolByName(school.getName());
        if (existingSchool.isPresent()) {
            throw new IllegalStateException("name taken");
        }
        School savedSchool = schoolRepository.save(school);
        System.out.println(savedSchool);
    }
}
